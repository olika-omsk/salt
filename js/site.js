function setFocus(a)
{
    document.getElementById(a).focus();
}

function toogle_menu(){
    if($(".header__mob-menu").hasClass("mob-hidden-menu")){
        $(".header__mob-menu").addClass('mob-visible-menu');
        $(".header__mob-menu").removeClass('mob-hidden-menu');
    }else{
        $(".header__mob-menu").addClass('mob-hidden-menu');
        $(".header__mob-menu").removeClass('mob-visible-menu');
    }
};


$(function(){
    $('#scroll-link').on('click', function(e){
        $('html,body').stop().animate({ scrollTop: $('#what-we-do').offset().top }, 1000);
        e.preventDefault();
    });
});


$(window).load(function(){
    setFocus("menu-home");
});